///<reference path='../_declare/google.maps.d.ts'/>
///<reference path='../core/Events.ts'/>

module services {
    import Events = core.Events;

    export class LocationService {
        private static instance:LocationService = null;

        events = new Events();

        private bounds:google.maps.LatLngBounds;

        constructor() {
            if(LocationService.instance){
                throw new Error('Instantiation failed: use LocationService.getInstance() instead of "new" keyword.');
            }
            LocationService.instance = this;
        }

        static getInstance():LocationService {
            if(LocationService.instance === null) {
                LocationService.instance = new LocationService();
            }
            return LocationService.instance;
        }

        setBounds(bounds:google.maps.LatLngBounds) {
            this.bounds = bounds;
            this.events.trigger('nm:changeBounds', bounds);
        }

        getBounds():google.maps.LatLngBounds {
            return this.bounds;
        }
    }
}