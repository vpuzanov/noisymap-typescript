///<reference path='../core/Events.ts'/>

module services {
    import Events = core.Events;

    export class FilterService {
        private static instance:FilterService = null;

        events = new Events();

        private filter:string;

        constructor() {
            if(FilterService.instance){
                throw new Error('Instantiation failed: use FilterService.getInstance() instead of "new" keyword.');
            }
            FilterService.instance = this;
        }

        static getInstance():FilterService {
            if(FilterService.instance === null) {
                FilterService.instance = new FilterService();
            }
            return FilterService.instance;
        }

        setFilter(filter:string) {
            this.filter = filter;
            this.events.trigger('nm:changeFilter', this.filter);
        }

        getFilter():string {
            return this.filter;
        }
    }
}