///<reference path='../../utils/TemplateFactory.ts'/>
///<reference path='../../core/BaseView.ts'/>
///<reference path='../../core/IInnerViewMap.ts'/>
///<reference path='../../posts/collections/PostCollection.ts'/>
///<reference path='../../posts/views/PostList.ts'/>
///<reference path='../../common/views/MapSection.ts'/>
///<reference path='../models/PlaceModel.ts'/>

module places {
    import TemplateFactory = utils.TemplateFactory;
    import BaseView = core.BaseView;
    import IInnerViewMap = core.IInnerViewMap;
    import MapSection = common.MapSection;
    import PostCollection = posts.PostCollection;
    import PostList = posts.PostList;

    export class PlaceDetails extends BaseView<PlaceModel> {
        private postCollection = new PostCollection();

        private $mapSection:JQuery;
        private $postsContainer:JQuery;

        protected innerViews:IInnerViewMap = {
            postList: new PostList(this.postCollection),
            mapSection: new MapSection(this.placeModel)
        };

        private postListEl = this.innerViews['postList'].render().el;

        constructor(private placeModel:PlaceModel) {
            super(placeModel);

            this.loadPosts();
        }

        render() {
            var placeData = this.placeModel.toJSON();
            placeData.place_ffm_id = parseInt(placeData.place_ffm_id, 10);
            super.render($(TemplateFactory.create('templates/places/PlaceDetailsTemplate', placeData)));

            this.$mapSection = this.$('.nm-Place-map');
            this.$el.prepend(this.innerViews['mapSection'].render().el);
            (<MapSection>this.innerViews['mapSection']).setHeight(80);

            this.$postsContainer = this.$('.nm-PlaceDetails-posts');
            this.$postsContainer.append(this.postListEl);

            return this;
        }

        private loadPosts() {
            this.postCollection.reset();
            this.postCollection.fetch({
                data: {
                    placeUuid: this.placeModel.getId()
                }
            });
        }
    }
}