///<reference path='../../core/BaseController.ts'/>
///<reference path='../models/PlaceModel.ts'/>
///<reference path='../views/PlaceDetails.ts'/>

module places {
    import BaseController = core.BaseController;

    export class PlaceController extends BaseController {
        private $container = $('.nm-ContentWrapper');

        private static instance:PlaceController = null;

        showView(placeUuid:string) {
            var model = new PlaceModel({place_uuid: placeUuid});

            model.fetch().done(() => {
                if (!this.view) {
                    this.view = new PlaceDetails(model);
                }

                this.$container.append(this.view.render().el);
            });
        }

        static getInstance():PlaceController {
            if(PlaceController.instance === null) {
                PlaceController.instance = new PlaceController();
            }
            return PlaceController.instance;
        }
    }
}