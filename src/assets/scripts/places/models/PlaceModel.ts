///<reference path='../../core/BaseModel.ts'/>

module places {
    import BaseModel = core.BaseModel;
    import Config = config.Config;

    export class PlaceModel extends BaseModel {
        get url() {
            return Config.getServiceUrl('place.item') + this.id;
        }

        get idAttribute() {
            return 'place_uuid';
        }

        getId() {
            return this.get('place_uuid');
        }

        get latitude() {
            return this.get('place_lat');
        }

        get longitude() {
            return this.get('place_lng');
        }
    }
}