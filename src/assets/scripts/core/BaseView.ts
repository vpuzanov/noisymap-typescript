///<reference path='./BaseModel.ts'/>
///<reference path='./IInnerViewMap.ts'/>

module core {
    export class BaseView<T extends BaseModel> extends Backbone.View<T> {
        protected innerViews:IInnerViewMap = {};

        constructor(model?:T) {
            super(model);

            if (model) {
                this.listenTo(model, 'remove', this.remove.bind(this));
            }
        }

        remove() {
            _.each(this.innerViews, (view:BaseView<BaseModel>) => {
                view.remove();
            });

            return super.remove();
        }

        render($newEl?:JQuery) {
            if ($newEl) {
                var $oldEl = this.$el;
                this.setElement($newEl);
                $oldEl.replaceWith($newEl);
            }

            return super.render();
        }
    }
}