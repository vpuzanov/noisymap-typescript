module core {
    export class BaseModel extends Backbone.Model {
        fetch(options?:{data?:{actionType?:string}}) {
            options = options || {};
            options.data = options.data || {};
            options.data.actionType = 'service';

            return super.fetch(options);
        }
    }
}