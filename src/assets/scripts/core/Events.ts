module core {
    export class Events {
        private dispatcher = _.clone(Backbone.Events);

        on(eventName:string, callback:(...args:any[]) => void, context?:any):any {
            return this.dispatcher['on'].apply(this.dispatcher, arguments);
        }

        off(eventName?:string, callback?:(...args:any[]) => void, context?:any):any {
            return this.dispatcher['off'].apply(this.dispatcher, arguments);
        }

        trigger(eventName:string, ...args:any[]):any {
            return this.dispatcher['trigger'].apply(this.dispatcher, arguments);
        }

        once(events:string, callback:Function):any {
            return this.dispatcher['once'].apply(this.dispatcher, arguments);
        }

        listenTo(object:any, events:string, callback:Function):any {
            return this.dispatcher['listenTo'].apply(this.dispatcher, arguments);
        }

        stopListening(object?:any, events?:string, callback?:Function):any {
            return this.dispatcher['stopListening'].apply(this.dispatcher, arguments);
        }
    }
}