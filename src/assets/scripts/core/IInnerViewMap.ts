///<reference path='./BaseModel.ts'/>
///<reference path='./BaseView.ts'/>

module core {
    export interface IInnerViewMap {
        [name:string]: BaseView<BaseModel>
    }
}