///<reference path='../../core/BaseView.ts'/>
///<reference path='../../utils/TemplateFactory.ts'/>
///<reference path='../collections/PostCollection.ts'/>
///<reference path='../models/PostModel.ts'/>
///<reference path='./Post.ts'/>

module posts {
    import BaseView = core.BaseView;
    import TemplateFactory = utils.TemplateFactory;

    export class PostList extends BaseView<any> {
        private $loadingIndicator:JQuery;
        private $noPostsFound:JQuery;

        constructor(public collection:PostCollection) {
            super();
            
            this.listenTo(this.collection, 'add', this.addPost.bind(this));
            this.listenTo(this.collection, 'request', this.onSyncStart);
            this.listenTo(this.collection, 'sync', this.onSyncEnd);
        }

        render() {
            super.render($(TemplateFactory.create('templates/posts/PostListTemplate')));

            this.$loadingIndicator = this.$('.nm-PostList-loadingIndicator');
            this.$noPostsFound = this.$('.nm-PostList-noPostsFound');

            return this;
        }

        private addPost(model:PostModel) {
            var post = new Post(model);
            this.$el.append(post.render().el);
        }

        private onSyncStart() {
            this.$noPostsFound.hide();
            this.$loadingIndicator.show();
        }

        private onSyncEnd() {
            this.$loadingIndicator.hide();

            if (this.collection.length === 0) {
                this.$noPostsFound.show();
            } else {
                this.$noPostsFound.hide();
            }
        }
    }
}