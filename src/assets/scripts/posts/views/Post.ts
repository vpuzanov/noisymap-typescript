///<reference path='../../core/BaseView.ts'/>
///<reference path='../../utils/TemplateFactory.ts'/>
///<reference path='../models/PostModel.ts'/>

module posts {
    import BaseView = core.BaseView;
    import TemplateFactory = utils.TemplateFactory;

    export class Post extends BaseView<PostModel> {
        constructor(private postModel:PostModel) {
            super(postModel);
        }

        render() {
            super.render($(TemplateFactory.create('templates/posts/PostTemplate', this.postModel.toJSON())));
            return this;
        }
    }
}