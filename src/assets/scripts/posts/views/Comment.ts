///<reference path='../../core/BaseView.ts'/>
///<reference path='../../utils/TemplateFactory.ts'/>
///<reference path='../models/CommentModel.ts'/>

module posts {
    import BaseView = core.BaseView;
    import TemplateFactory = utils.TemplateFactory;

    export class Comment extends BaseView<CommentModel> {
        constructor(private commentModel:CommentModel) {
            super(commentModel);
        }

        render() {
            super.render($(TemplateFactory.create('templates/posts/CommentTemplate', this.commentModel.toJSON())));
            return this;
        }
    }
}