///<reference path='../../utils/TemplateFactory.ts'/>
///<reference path='../../core/BaseView.ts'/>
///<reference path='../../core/IInnerViewMap.ts'/>
///<reference path='../models/PostModel.ts'/>
///<reference path='./CommentList.ts'/>

module posts {
    import TemplateFactory = utils.TemplateFactory;
    import BaseView = core.BaseView;
    import IInnerViewMap = core.IInnerViewMap;

    export class PostDetails extends BaseView<PostModel> {
        private $posts:JQuery;
        private $comments:JQuery;

        protected innerViews:IInnerViewMap = {
            postSection: new Post(this.postModel),
            commentsSection: new CommentList(this.postModel.comments)
        };

        constructor(private postModel:PostModel) {
            super(postModel);
        }

        render() {
            super.render($(TemplateFactory.create('templates/posts/PostDetailsTemplate', this.postModel.toJSON())));

            this.$posts = this.$('.nm-PostDetails-posts');
            this.$comments = this.$('.nm-PostDetails-comments');

            this.$posts.append(this.innerViews['postSection'].render().el);
            this.$comments.append(this.innerViews['commentsSection'].render().el);

            return this;
        }
    }
}