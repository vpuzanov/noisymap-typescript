///<reference path='../../core/BaseView.ts'/>
///<reference path='../../utils/TemplateFactory.ts'/>
///<reference path='../collections/CommentCollection.ts'/>
///<reference path='../models/CommentModel.ts'/>
///<reference path='./Comment.ts'/>

module posts {
    import BaseView = core.BaseView;
    import TemplateFactory = utils.TemplateFactory;

    export class CommentList extends BaseView<any> {
        constructor(public collection:CommentCollection) {
            super();

            this.listenTo(this.collection, 'add', this.addComment.bind(this));
        }

        render() {
            super.render($(TemplateFactory.create('templates/posts/CommentListTemplate')));

            this.collection.each(this.addComment.bind(this));

            return this;
        }

        private addComment(model:CommentModel) {
            var comment = new Comment(model);
            this.$el.append(comment.render().el);
        }
    }
}