///<reference path='../../services/FilterService.ts'/>
///<reference path='../../core/BaseView.ts'/>
///<reference path='../../utils/TemplateFactory.ts'/>

module posts {
    import BaseView = core.BaseView;
    import TemplateFactory = utils.TemplateFactory;
    import FilterService = services.FilterService;

    export class Filter extends BaseView<any> {
        private filterService = FilterService.getInstance();

        constructor() {
            super();

            this.listenTo(this.filterService.events, 'nm:changeFilter', this.selectFilter.bind(this));
        }

        private selectFilter(filter:string) {
            filter = filter || 'all';
            this.$('.nm-Content-filter-item').removeClass('nm-Content-filterSelected');
            this.$('.nm-Content-filter-item_' + filter).addClass('nm-Content-filterSelected');
        }

        render() {
            super.render($(TemplateFactory.create('templates/posts/FilterTemplate')));

            this.selectFilter(this.filterService.getFilter());

            return this;
        }
    }
}