///<reference path='../models/CommentModel.ts'/>
///<reference path='../../core/BaseCollection.ts'/>

module posts {
    import BaseCollection = core.BaseCollection;

    export class CommentCollection extends BaseCollection<CommentModel> {
        get model() {
            return CommentModel;
        }
    }
}