///<reference path='../models/PostModel.ts'/>
///<reference path='../../core/BaseCollection.ts'/>

module posts {
    import BaseCollection = core.BaseCollection;
    import Config = config.Config;

    export class PostCollection extends BaseCollection<PostModel> {
        url = Config.getServiceUrl('post.list');

        get model() {
            return PostModel;
        }
    }
}