///<reference path='../../core/BaseController.ts'/>
///<reference path='../models/PostModel.ts'/>
///<reference path='../views/PostDetails.ts'/>

module posts {
    import BaseController = core.BaseController;

    export class PostController extends BaseController {
        private $container = $('.nm-ContentWrapper');

        private static instance:PostController = null;

        showView(postUuid:string) {
            var model = new PostModel({post_uuid: postUuid});

            model.fetch().done(() => {
                if (!this.view) {
                    this.view = new PostDetails(model);
                }

                this.$container.append(this.view.render().el);
            });
        }

        static getInstance():PostController {
            if(PostController.instance === null) {
                PostController.instance = new PostController();
            }
            return PostController.instance;
        }
    }
}