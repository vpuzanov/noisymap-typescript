///<reference path='../../core/BaseModel.ts'/>

module posts {
    import BaseModel = core.BaseModel;

    export class CommentModel extends BaseModel {
        get idAttribute() {
            return 'comment_uuid';
        }
    }
}