///<reference path='../../core/BaseModel.ts'/>
///<reference path='../collections/CommentCollection.ts'/>

module posts {
    import BaseModel = core.BaseModel;
    import Config = config.Config;

    export class PostModel extends BaseModel {
        get url() {
            return Config.getServiceUrl('post.item') + this.id;
        }

        get idAttribute() {
            return 'post_uuid';
        }

        get comments():CommentCollection {
            var comments = this.get('comments');
            return comments instanceof CommentCollection ? comments : new CommentCollection(comments);
        }
    }
}