///<reference path='../../services/LocationService.ts'/>
///<reference path='../../services/FilterService.ts'/>
///<reference path='../../core/BaseController.ts'/>
///<reference path='../../posts/collections/PostCollection.ts'/>
///<reference path='../../posts/models/PostModel.ts'/>
///<reference path='../views/Home.ts'/>

module home {
    import LocationService = services.LocationService;
    import FilterService = services.FilterService;
    import BaseController = core.BaseController;
    import PostCollection = posts.PostCollection;
    import PostModel = posts.PostModel;

    export class HomeController extends BaseController {
        private $container = $('.nm-ContentWrapper');
        private postCollection = new PostCollection();

        private static instance:HomeController = null;

        private locationService = LocationService.getInstance();
        private filterService = FilterService.getInstance();

        private loadPostsTimeoutHandle = null;

        showView(filter:string = '') {
            if (!this.view) {
                this.view = new Home(this.postCollection);
                this.$container.append(this.view.render().el);
            }

            this.view.listenTo(this.locationService.events, 'nm:changeBounds', (bounds:google.maps.LatLngBounds) => {
                clearTimeout(this.loadPostsTimeoutHandle);

                this.loadPostsTimeoutHandle = setTimeout(() => {
                    this.loadPosts(filter, bounds);
                }, 1000);
            });

            this.loadPosts(filter);
        }

        private loadPosts(filter:string, bounds?:google.maps.LatLngBounds) {
            var currentFilter = this.filterService.getFilter();
            if (bounds === undefined) {
                if (currentFilter === undefined) {
                    return;
                } else {
                    bounds = this.locationService.getBounds();
                }
            }

            this.filterService.setFilter(filter);

            this.postCollection.reset();
            this.postCollection.fetch({
                data: {
                    bounds: bounds.toUrlValue(),
                    filter: filter
                }
            });
        }

        static getInstance():HomeController {
            if(HomeController.instance === null) {
                HomeController.instance = new HomeController();
            }
            return HomeController.instance;
        }

        removeView() {
            this.filterService.setFilter(undefined);
            return super.removeView();
        }
    }
}