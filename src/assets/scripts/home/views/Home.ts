///<reference path='../../utils/TemplateFactory.ts'/>
///<reference path='../../core/BaseView.ts'/>
///<reference path='../../common/views/MapSection.ts'/>
///<reference path='../../posts/views/PostList.ts'/>
///<reference path='../../posts/views/Filter.ts'/>
///<reference path='../../posts/collections/PostCollection.ts'/>

module home {
    import TemplateFactory = utils.TemplateFactory;
    import BaseView = core.BaseView;
    import IInnerViewMap = core.IInnerViewMap;
    import MapSection = common.MapSection;
    import PostCollection = posts.PostCollection;
    import PostList = posts.PostList;
    import Filter = posts.Filter;

    export class Home extends BaseView<any> {
        private $map:JQuery;
        private $filter:JQuery;
        private $content:JQuery;

        protected innerViews:IInnerViewMap = {
            mapSection: new MapSection(),
            filterSection: new Filter(),
            contentSection: new PostList(this.postCollection)
        };

        constructor(private postCollection?:PostCollection) {
            super();
        }

        render() {
            super.render($(TemplateFactory.create('templates/common/HomeTemplate')));

            this.$map = this.$('.nm-Map');
            this.$map.html(this.innerViews['mapSection'].render().el);

            this.$filter = this.$('.nm-Content-filterWrapper');
            this.$filter.html(this.innerViews['filterSection'].render().el);

            this.$content = this.$('.nm-Content-inner');
            this.$content.append(this.innerViews['contentSection'].render().el);

            return this;
        }
    }
}