///<reference path='../../utils/TemplateFactory.ts'/>
///<reference path='../../core/BaseView.ts'/>
///<reference path='../../core/IInnerViewMap.ts'/>
///<reference path='../../posts/collections/PostCollection.ts'/>
///<reference path='../../posts/views/PostList.ts'/>
///<reference path='../models/UserModel.ts'/>

module users {
    import TemplateFactory = utils.TemplateFactory;
    import BaseView = core.BaseView;
    import IInnerViewMap = core.IInnerViewMap;
    import PostCollection = posts.PostCollection;
    import PostList = posts.PostList;

    export class UserDetails extends BaseView<UserModel> {
        private postCollection = new PostCollection();

        private $postsContainer:JQuery;

        protected innerViews:IInnerViewMap = {
            postList: new PostList(this.postCollection)
        };

        private postListEl = this.innerViews['postList'].render().el;

        constructor(private userModel:UserModel) {
            super(userModel);

            this.loadPosts();
        }

        render() {
            super.render($(TemplateFactory.create('templates/users/UserDetailsTemplate', this.userModel.toJSON())));

            this.$postsContainer = this.$('.nm-UserDetails-posts');
            this.$postsContainer.append(this.postListEl);

            return this;
        }

        private loadPosts() {
            this.postCollection.reset();
            this.postCollection.fetch({
                data: {
                    userUuid: this.userModel.getId()
                }
            });
        }
    }
}