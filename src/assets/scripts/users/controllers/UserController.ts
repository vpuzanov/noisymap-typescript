///<reference path='../../core/BaseController.ts'/>
///<reference path='../models/UserModel.ts'/>
///<reference path='../views/UserDetails.ts'/>

module users {
    import BaseController = core.BaseController;

    export class UserController extends BaseController {
        private $container = $('.nm-ContentWrapper');

        private static instance:UserController = null;

        showView(userUuid:string) {
            var model = new UserModel({user_uuid: userUuid});

            model.fetch().done(() => {
                if (!this.view) {
                    this.view = new UserDetails(model);
                }

                this.$container.append(this.view.render().el);
            });
        }

        static getInstance():UserController {
            if(UserController.instance === null) {
                UserController.instance = new UserController();
            }
            return UserController.instance;
        }
    }
}