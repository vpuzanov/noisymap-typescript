///<reference path='../../core/BaseModel.ts'/>

module users {
    import BaseModel = core.BaseModel;
    import Config = config.Config;

    export class UserModel extends BaseModel {
        get url() {
            return Config.getServiceUrl('user.item') + this.id;
        }

        get idAttribute() {
            return 'user_uuid';
        }

        getId() {
            return this.get('user_uuid');
        }
    }
}