module config {
    export class Config {
        private static services = {
            'post.list': 'http://mapdev/?action=post.list',
            'post.item': 'http://mapdev/?action=post.item&uuid=',
            'place.item': 'http://mapdev/?action=place.item&uuid=',
            'user.item': 'http://mapdev/?action=user.item&uuid='
        };

        static getServiceUrl(service:string):string {
            return Config.services[service];
        }
    }
}