module config {
    export class Config {
        private static services = {
            'post.list': 'http://dev.noisymap.com/?action=post.list',
            'post.item': 'http://dev.noisymap.com/?action=post.item&uuid=',
            'place.item': 'http://dev.noisymap.com/?action=place.item&uuid=',
            'user.item': 'http://dev.noisymap.com/?action=user.item&uuid='
        };

        static getServiceUrl(service:string):string {
            return Config.services[service];
        }
    }
}