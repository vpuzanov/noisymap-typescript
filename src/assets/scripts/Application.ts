///<reference path='./_declare/external.d.ts'/>
///<reference path='./_declare/backbone.d.ts'/>
///<reference path='./routing/Router.ts'/>
///<reference path='./core/BaseView.ts'/>
///<reference path='./utils/TemplateFactory.ts'/>
///<reference path='./common/views/Header.ts'/>
///<reference path='./common/views/Footer.ts'/>

module application {
    import BaseView = core.BaseView;
    import TemplateFactory = utils.TemplateFactory;
    import Header = common.Header;
    import Footer = common.Footer;

    export class Application extends BaseView<any> {
        private $container = $('#nm-App');

        constructor() {
            super();

            this.$container.append(this.render().el, new Footer().render().el);
            new Router();
        }

        render() {
            super.render($(TemplateFactory.create('templates/ApplicationTemplate')));
            this.$el.prepend(new Header().render().el);

            return this;
        }
    }
}