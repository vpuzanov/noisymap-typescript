///<reference path='../home/controllers/HomeController.ts'/>
///<reference path='../posts/controllers/PostController.ts'/>
///<reference path='../users/controllers/UserController.ts'/>
///<reference path='../places/controllers/PlaceController.ts'/>

module application {
    import HomeController = home.HomeController;
    import PostController = posts.PostController;
    import UserController = users.UserController;
    import PlaceController = places.PlaceController;

    export class Router extends Backbone.Router {
        private viewController;

        constructor() {
            this.routes = {
                '(filter/:filter)': this.showHome,
                'posts/:postUuid': this.showPost,
                'users/:userUuid': this.showUser,
                'places/:placeUuid': this.showPlace
            };

            super();

            Backbone.history.start();
        }

        private showHome(filter?:string) {
            this.loadView(HomeController, filter);
        }

        private showPost(postUuid:string) {
            this.loadView(PostController, postUuid);
        }

        private showUser(userUuid:string) {
            this.loadView(UserController, userUuid);
        }

        private showPlace(placeUuid:string) {
            this.loadView(PlaceController, placeUuid);
        }

        private loadView(Controller, ...args) {
            if (this.viewController && !(this.viewController instanceof Controller)) {
                this.viewController.removeView();
            }

            this.viewController = Controller.getInstance();
            this.viewController.showView.apply(this.viewController, args);
        }
    }
}