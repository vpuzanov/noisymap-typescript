///<reference path='../../core/BaseView.ts'/>
///<reference path='../../utils/TemplateFactory.ts'/>

module common {
    import BaseView = core.BaseView;
    import TemplateFactory = utils.TemplateFactory;

    export class Header extends BaseView<any> {
        render() {
            super.render($(TemplateFactory.create('templates/common/HeaderTemplate')));

            return this;
        }
    }
}