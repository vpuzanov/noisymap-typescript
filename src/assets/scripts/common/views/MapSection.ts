///<reference path='../../_declare/google.maps.d.ts'/>

///<reference path='../../core/BaseView.ts'/>
///<reference path='../../utils/TemplateFactory.ts'/>
///<reference path='../../services/LocationService.ts'/>
///<reference path='../../places/models/PlaceModel.ts'/>
///<reference path='../configs/MapOptions.ts'/>

module common {
    import BaseView = core.BaseView;
    import TemplateFactory = utils.TemplateFactory;
    import LocationService = services.LocationService;
    import PlaceModel = places.PlaceModel;

    export class MapSection extends BaseView<PlaceModel> {
        private map;
        private $mapEl:JQuery;
        private mapOptions = MapOptions;

        private static CLIENT_LOCATION_ZOOM = 12;

        private locationService = LocationService.getInstance();

        constructor(private placeModel?:PlaceModel) {
            super();
        }

        render() {
            super.render($(TemplateFactory.create('templates/common/MapSectionTemplate')));

            this.initMap().done((map) => {
                this.map = map;

                google.maps.event.addListener(this.map, 'bounds_changed', () => {
                    this.locationService.setBounds(this.map.getBounds());
                });
            });

            return this;
        }

        private initMap() {
            var deferred = jQuery.Deferred();

            this.$mapEl = this.$('#Map');

            if (this.placeModel) {
                this.mapOptions.center = new google.maps.LatLng(this.placeModel.latitude, this.placeModel.longitude);
                this.mapOptions.zoom = MapSection.CLIENT_LOCATION_ZOOM;
                deferred.resolve(new google.maps.Map(this.$mapEl.get(0), this.mapOptions));
            } else {
                if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition((position) => {
                        this.mapOptions.center = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
                        this.mapOptions.zoom = MapSection.CLIENT_LOCATION_ZOOM;
                        deferred.resolve(new google.maps.Map(this.$mapEl.get(0), this.mapOptions));
                    });
                } else {
                    deferred.resolve(new google.maps.Map(this.$mapEl.get(0), this.mapOptions));
                }
            }

            return deferred.promise();
        }

        setHeight(height:number) {
            this.$mapEl.height(height);
        }

        remove() {
            google.maps.event.clearInstanceListeners(window);
            google.maps.event.clearInstanceListeners(document);
            google.maps.event.clearInstanceListeners(this.$mapEl.get(0));
            this.$mapEl.remove();
            this.map = null;

            return super.remove();
        }
    }
}