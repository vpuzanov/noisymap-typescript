///<reference path='../../_declare/google.maps.d.ts'/>

module common {
    export var MapOptions = {
        scrollwheel: false,
        zoom: 3,
        center: new google.maps.LatLng(54.521081, 15.292969),
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        panControl: false,
        zoomControl: true,
        zoomControlOptions: {
            position: google.maps.ControlPosition.LEFT_TOP
        },
        mapTypeControl: true,
        mapTypeControlOptions: {
            position: google.maps.ControlPosition.TOP_RIGHT
        },
        scaleControl: false,
        streetViewControl: false,
        overviewMapControl: false
    }
}