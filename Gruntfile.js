module.exports = function(grunt) {
    require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);

    var configFilePath = 'src/assets/scripts/config/Config.ts';
    var server = grunt.option('server') || 'local';
    if (server === 'remote') {
        configFilePath = 'src/assets/scripts/config/ConfigRemote.ts'
    }

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        BASE_PATH: '',
        DEVELOPMENT_PATH: 'src/',
        PRODUCTION_PATH: 'web/',
        BOWER_PATH: 'bower_components/',

        env: {
            src: {
                NODE_ENV : 'DEVELOPMENT',
                ASSETS_ROOT: ''
            },
            web : {
                NODE_ENV : 'PRODUCTION',
                ASSETS_ROOT: '../src/'
            }
        },

        preprocess : {
            src : {
                src : '<%= DEVELOPMENT_PATH %>' + 'config.html',
                dest : '<%= DEVELOPMENT_PATH %>' + 'index.html',
                options : {
                    context : {
                        appVersion : '<%= pkg.version %>',
                        filePath: ''
                    }
                }
            },
            web : {
                src : '<%= DEVELOPMENT_PATH %>' + 'config.html',
                dest : '<%= PRODUCTION_PATH %>' + 'index.html',
                options : {
                    context : {
                        appVersion : '<%= pkg.version %>',
                        filePath: ''
                    }
                }
            }
        },

        clean: {
            web: ['<%= PRODUCTION_PATH %>'],
            docs: ['<%= BASE_PATH %>docs'],
            temp: ['.tmp']
        },

        copy: {
            web:  {
                files: [
                    { expand: true, cwd: '<%= DEVELOPMENT_PATH %>', src: 'favicon.ico', dest: '<%= PRODUCTION_PATH %>' },
                    { expand: true, cwd: '<%= DEVELOPMENT_PATH %>', src: ['assets/media/**'], dest: '<%= PRODUCTION_PATH %>' },
                    { expand: true, cwd: '<%= DEVELOPMENT_PATH %>', src: ['resources/**'], dest: '<%= PRODUCTION_PATH %>' }
                ]
            }
        },

        bumpup: {
            options: {
                updateProps: {
                    pkg: 'package.json'
                }
            },
            file: 'package.json'
        },

        json: {
            web: {
                options: {
                    namespace: 'JSON_DATA',
                    includePath: false,
                    processName: function(filename) {
                        return filename.toLowerCase();
                    }
                },
                src: ['<%= DEVELOPMENT_PATH %>' + 'assets/data/**/*.json'],
                dest:  '<%= DEVELOPMENT_PATH %>' + 'assets/scripts/compiled/json.js'
            }
        },

        handlebars: {
            compile: {
                options: {
                    namespace: 'JST',
                    processName: function(filePath) {
                        return filePath.slice(filePath.indexOf('template'), filePath.lastIndexOf('.'));
                    }
                },
                files: {
                    '<%= DEVELOPMENT_PATH %>assets/scripts/compiled/templates.tmpl.js': ['<%= DEVELOPMENT_PATH %>' + 'assets/templates/**/*.hbs']
                }
            }
        },

        typescript: {
            main: {
                src: [configFilePath, '<%= DEVELOPMENT_PATH %>' + 'assets/scripts/Application.ts'],
                dest: '<%= DEVELOPMENT_PATH %>' + 'assets/scripts/compiled/app.js',
                options: {
                    target: 'es5',
                    sourceMap: true,
                    declaration: false,
                    noLib: false,
                    removeComments: true
                }
            }
        },

        wiredep: {
            html: {
                src: ['<%= PRODUCTION_PATH %>' + 'index.html'],
                options: {
                    devDependencies: true
                }
            }
        },

        useminPrepare: {
            html: ['<%= PRODUCTION_PATH %>' + 'index.html'],
            options: {
                dest: '<%= PRODUCTION_PATH %>'
            }
        },
        usemin: {
            html: ['<%= PRODUCTION_PATH %>' + 'index.html']
        },

        htmlmin: {
            dist: {
                options: {
                    removeComments: true,
                    collapseWhitespace: false
                },
                files: {
                    '<%= PRODUCTION_PATH %>index.html': '<%= PRODUCTION_PATH %>' + 'index.html'
                }
            }
        },

        concurrent: {
            src: ['json', 'handlebars', 'typescript']
        },

        express: {
            src: {
                options: {
                    port: 8000,
                    hostname: '0.0.0.0',
                    bases: ['<%= DEVELOPMENT_PATH %>', '<%= BOWER_PATH %>'],
                    livereload: true
                }
            },
            web: {
                options: {
                    port: 8001,
                    hostname: '0.0.0.1',
                    bases: ['<%= PRODUCTION_PATH %>']
                }
            }
        },

        open: {
            src: {
                path: 'http://localhost:<%= express.src.options.port%>'
            },
            web: {
                path: 'http://localhost:<%= express.web.options.port%>'
            }
        },

        watch: {
            css: {
                options: {
                    livereload: true
                },
                files: [
                    '<%= DEVELOPMENT_PATH %>' + 'assets/styles/**/*.css'
                ]
            },
            src: {
                options: {
                    livereload: true
                },
                files: [
                    '<%= DEVELOPMENT_PATH %>' + 'assets/scripts/**/*.ts',
                    '<%= DEVELOPMENT_PATH %>' + 'config.html',
                    '<%= DEVELOPMENT_PATH %>' + 'assets/templates/**/*.hbs'
                ],
                tasks: ['src']
            }
        }

    });

    /**
     * Grunt tasks:
     *
     * grunt        (Builds and runs development code/server)
     * grunt web    (Builds and runs production code/server)
     * grunt build  (Builds the production code but does not start a local server)
     */
    grunt.registerTask('default', [
        'server'
    ]);

    grunt.registerTask('server', [
        'src',
        'express:src',
        'open:src',
        'watch'
    ]);

    grunt.registerTask('src', [
        'env:src',
        'preprocess:src',
        'json',
        'handlebars',
        'typescript'
    ]);

    grunt.registerTask('web', [
        'build',
        'open:web',
        'express:web',
        'express-keepalive'
    ]);

    grunt.registerTask('build', [
        'clean',
        'bumpup',
        'env:web',
        'preprocess:web',
        'wiredep',
        'json',
        'handlebars',
        'typescript',
        'copy',
        'useminPrepare', 'concat:generated', 'uglify:generated', 'cssmin:generated',
        'usemin',
        'htmlmin'
    ]);
};