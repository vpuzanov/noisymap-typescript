### Setup instructions

You must have [Node.js](https://nodejs.org/) installed to build and run this project.

Setup steps:

1. Clone repository
2. In project root directory run: `npm install`
3. In project root directory run: `bower install`
4. In project root directory run: `grunt --server=remote` (this will build the development code, start the development server and open the project in a new browser window)

#### Grunt tasks
This project has several Grunt tasks registered:

1. `grunt` - builds and runs development code on development server
2. `grunt web` - builds and runs production code (concatenated and uglyfied) on production server
3. `grunt build` - builds production code (concatenated and uglyfied) but does not run it

#### Grunt options

When running Grunt tasks described above you should always specify Grunt option `--server=remote` to be able to use NoisyMap back-end services.